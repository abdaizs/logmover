'''
Created on 2013.08.25.

@author: Zsolt Abdai
'''

if __name__ == '__main__':
    pass

import logging
import os
import errno
import shutil
import gzip
import sys

default_log_name = '/srv/logmover.log'
default_cfg_path = '/srv/logmover.conf'

if (len(sys.argv) == 3):
    default_log_name = sys.argv[1]
    default_cfg_path = sys.argv[2]
else:
    print("Nem jol adtad meg a config parametert!")

logging.basicConfig(filename=default_log_name,level=logging.INFO,format='%(asctime)s %(levelname)s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')

#Configuration's variables
c_filepath = ""
c_exclude = ""
c_archpath = ""
c_zipfiles = False

logging.info("Read the configuration data started...");

try:
    config_file = open(default_cfg_path, "r")
    #Set variables from configuration file
    for config_data in config_file:
        if (config_data.split('=')[0] == 'filepath'):
            c_filepath = str(config_data.split('=')[1].rstrip('\n'))
        if (config_data.rsplit('=')[0] == 'archpath'):
            c_archpath = str(config_data.split('=')[1].rstrip('\n'))
        if (config_data.rsplit('=')[0] == 'exclude'):
            c_exclude = str(config_data.split('=')[1].rstrip('\n'))
        if (config_data.rsplit('=')[0] == 'zipfiles'):
            c_zipfiles = (str(config_data.split('=')[1].rstrip('\n')) == 'ON')
    logging.info("Read configuration data ended...")
    config_file.close()
    logging.info("Source: " + c_filepath)
    logging.info("Destination: " + c_archpath)
    logging.info("ZIP files: " + str(c_zipfiles))
    logging.info("Except: " + str(c_exclude))
except Exception, exception:
    if exception.errno == errno.EEXIST:
        pass
    else:
        logging.error("Can't set configuration!" + str(exception) + " - " + str(exception.errno))
        raise

try:  
    if not os.path.exists(c_filepath):
        logging.info("Create directory: " + c_filepath)
        os.makedirs(c_filepath, 0777)
    
    if not os.path.exists(c_archpath):
        logging.info("Create archive directory: " + c_archpath)
        os.makedirs(c_archpath, 0777)
except Exception, exception:
    if exception.errno == errno.EEXIST and os.path.isdir(c_filepath) and os.path.isdir(c_archpath):
        pass
    else:
        logging.error("Can't create directories! " + str(exception) + " - " + str(exception.errno))
        raise

#Get files from source directory and use gzip for file if zipping is enabled, than move to archive path
try:
    onlyfiles = [ f for f in os.listdir(c_filepath) if (os.path.isfile(os.path.join(c_filepath,f)))]
    for onefile in onlyfiles:
        if onefile not in c_exclude.rsplit(','):
            #File is not excluded, can be moved
            #zipping is enabled
            if c_zipfiles:
                logging.info("Start zipping file: " + onefile)
                f_archive_path = c_archpath + '/' + onefile  + '.gz'
                i = 1
                while os.path.isfile(f_archive_path):
                    f_archive_path = c_archpath + '/' + onefile + "_" + str(i)  + '.gz'
                    i = i + 1
                f_in = open(c_filepath + '/' + onefile, 'rb')
                f_out = gzip.open(f_archive_path, 'wb')
                f_out.writelines(f_in)
                f_out.close()
                f_in.close()
                os.remove(c_filepath + '/' + onefile)
                logging.info("End zipping file: " + onefile)
            #zipping is disabled
            else:
                logging.info("Start moving file: " + onefile)
                f_archive_path = c_archpath + "/" + onefile
                i = 1
                while os.path.isfile(f_archive_path):
                    logging.info(f_archive_path + " already exist, will be incremented!")
                    f_archive_path = c_archpath + '/' + onefile + "_" + str(i)
                    i = i + 1
                shutil.move(c_filepath + "/" +onefile, f_archive_path)
                logging.info("End moving file: " + onefile)
        else:
            #File is in exclude list, can't be moved
            logging.warning("The file is in exclude list, will not be moved: " + onefile)
except Exception, exception:
    if exception.errno != errno.EEXIST:
        logging.error("Exception occurred: " + str(exception) + " - " + str(exception.errno))
        raise

logging.info("Task completed successfull!")
